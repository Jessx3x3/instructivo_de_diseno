import 'package:flutter/material.dart';
import 'package:prototipo_projecto/inventory/listview_product.dart';

import 'login/loginScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Project Application APP',
      theme: ThemeData(
        primarySwatch: Colors.deepPurpleAccent[900],
      ),
      home: LoginScreen(),
    );
  }
}
