import 'package:flutter/material.dart';
import 'package:prototipo_projecto/inventory/listview_product.dart';
import 'package:intl/intl.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  TextEditingController _nombreController;
  bool _validate = false;
  var now = new DateTime.now();


  @override
  void dispose() {
    // TODO: implement dispose
    _nombreController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          wallpaper,
          Center(
            child: Column(
                children: <Widget>[
                 SizedBox(height: 250),
                  Text(new DateFormat("dd-MM-yyyy").format(now), style: TextStyle(color: Colors.white, fontSize: 17)),
                  SizedBox(height: 20 ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.deepPurple[900])
                    ),
                    padding: EdgeInsets.only(left: 40, top: 20, bottom: 20, right: 50),
                    color: Colors.deepPurple[800],
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ListViewProduct()));
                    },
                    child: const Text('Ingresar', style: TextStyle(fontSize: 18, color: Colors.white)),
                  ),
                  SizedBox(height: 20),
                ],
            )
          )
        ],
      )
    );
  }
  final wallpaper = new Container(
    decoration: new BoxDecoration(
      image: new DecorationImage(image: new AssetImage("assets/wallpaper_1.png"), fit: BoxFit.fill),
    ),
  );
  void _showAlertDialogEmpty() {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("Sin Nombre"),
            content: Text("Debe ingresar un nombre válido"),
            actions: <Widget>[
              RaisedButton(
                child: Text("Volver", style: TextStyle(color: Colors.white),),
                onPressed: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen())); },
                color: Colors.purple,
              )
            ],
          );
        }
    );
  }
}
