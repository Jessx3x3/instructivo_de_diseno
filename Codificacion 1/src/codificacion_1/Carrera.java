/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codificacion_1;

/**
 *
 * @author Jess
 */
public class Carrera {
    
    String nombre;
    String duracion;
    int codigo;

    public Carrera(String nombre, String duracion, int codigo) {
        this.nombre = nombre;
        this.duracion = duracion;
        this.codigo = codigo;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Carrera{" + "nombre=" + nombre + ", duracion=" + duracion + ", codigo=" + codigo + '}';
    }
    
    
}
