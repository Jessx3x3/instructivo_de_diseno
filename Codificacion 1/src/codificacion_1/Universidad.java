/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codificacion_1;

/**
 *
 * @author Jess
 */
public class Universidad {
    
    String nombre;
    String direccion;
    String trayectoria;

    public Universidad(String nombre, String direccion, String trayectoria) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.trayectoria = trayectoria;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTrayectoria() {
        return trayectoria;
    }

    public void setTrayectoria(String trayectoria) {
        this.trayectoria = trayectoria;
    }
    
    @Override
    public String toString() {
        return "Universidad{" + "nombre=" + nombre + ", direccion=" + direccion + ", trayectoria=" + trayectoria + '}';
    }
    
}
