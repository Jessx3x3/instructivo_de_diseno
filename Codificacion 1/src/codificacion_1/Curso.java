/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codificacion_1;

/**
 *
 * @author Jess
 */
public class Curso {
    
    int codigo;
    int horas;
    String nombre;

    public Curso(int codigo, int horas, String nombre) {
        this.codigo = codigo;
        this.horas = horas;
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void darPrueba(){
        
    }
    
    public void aprobarPrueba(){
        
    }
    
    @Override
    public String toString() {
        return "Curso{" + "codigo=" + codigo + ", horas=" + horas + ", nombre=" + nombre + '}';
    }
    
    
}
