/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codificacion_1.Persona;

/**
 *
 * @author Jess
 */
public class Estudiante extends Persona{
    
    int matricula;
    
    public Estudiante(String nombre, String apellido, String fechaNac, String run, String email, int matricula) {
        super(nombre, apellido, fechaNac, run, email);
        
        this.matricula = matricula;
    }
    
}
