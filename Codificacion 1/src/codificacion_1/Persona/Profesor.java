/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codificacion_1.Persona;

/**
 *
 * @author Jess
 */
public class Profesor extends Persona{
    
    String profesion;
    
    public Profesor(String nombre, String apellido, String fechaNac, String run, String email, String profesion) {
        super(nombre, apellido, fechaNac, run, email);
        
        this.profesion = profesion;
    }
    
}
