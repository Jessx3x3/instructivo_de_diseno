package com.example.tienda.products;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Clase realizada con el fin de crear m�todos de manejo en la base de datos, como lo son: ver, agregar y eliminar datos.
 * @author Jess
 * @version 1.0
 */

@RestController
public class ProductController {

	@Autowired
	private ProductRepository productRepo;
	
	@Autowired
	private ProductServices productServices;
	
     // @return M�todo creado para ver el listado total de productos a trav�s de /products 
	
	@GetMapping("/products")
	public List<Products> getProducts() {
		
		return productRepo.findAll();
	}
	
	 // @param productName, atributo necesario para encontrar un producto por su nombre
	 // @return M�todo creado para obtener producto por nombre a trav�s de /products/nombreProducto
	
	@GetMapping("/products/{name}")
	public Products obtener(@PathVariable("name") String productName) {
		return productRepo.findByProductName(productName);
	}
	
	// @param product, objeto necesario para agregar a base de datos
	// @return M�todo creado para agregar productos a la base de datos. Esto, a trav�s de POST en /addProduct m�s los valores y atributos necesarios del producto
	
	@PostMapping("/addProduct")
	public Products agregarProduct(@RequestBody Products product) {
		return productRepo.save(product);
	}
    
    // M�todo creado para eliminar productos de la base de datos. Esto, a trav�s de DELETE en /deleteProducts/id. Para eliminar es necesario contar con el id del producto
    // @param productId, id necesario para eliminar un producto en espec�fico
	
	@DeleteMapping("/deleteProducts/{id}")
	public void eliminarProducto(@PathVariable("id") Long productId) {
		productRepo.deleteById(productId);
	}
	
	//M�todo a testear
	@GetMapping("/lista")
	public ArrayList<String> listar() {
		return productServices.listar();
	}
}
