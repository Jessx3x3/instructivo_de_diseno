package com.example.tienda.products;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface que define la b�squeda de productos por su nombre
 * @author Jess
 *
 */

public interface ProductRepository extends JpaRepository<Products, Long> {
	Products findByProductName(String productName);
}
