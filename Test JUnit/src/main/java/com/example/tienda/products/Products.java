package com.example.tienda.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Esta clase contiene los atributos propios de un producto básico. Estos son: Nombre, Marca, Stock y ID respectivo para identificarlo. 
 * Cada atributo contiene sus respectivos getters and setters. Trabajarémos con estos atributos y clase a la hora de agregar productos a la base de datos.
 * @author Jess
 * @version 1.0
 */

@Entity
public class Products {

	//ID generado automáticamente
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//Cambio de nombre porque tabla no soporta la sintaxis de productName
	@Column(name = "product_name")
	private String productName;
	
	@Column
	private String brand;
	
	@Column
	private int stock;
	
	public Products() {

	}
	
	/*
	 * Constructor. Incluye como parámetros los atributos de id, name, brand y stock
	 */
	public Products(Long id, String productName, String brand, int stock) {
		super();
		this.id = id;
		this.productName = productName;
		this.brand = brand;
		this.stock = stock;
	}
	
	/**
	 * @param id: identificador producto, tipo long. Generado automáticamente
	 * @param productName: nombre del producto, tipo String.
	 * @param brand: marca del producto, tipo String.
	 * @param stock: cantidad disponible de producto, tipo int.
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
	

}
