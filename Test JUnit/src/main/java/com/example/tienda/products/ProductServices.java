package com.example.tienda.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.*;
import java.util.List;


@Service
public class ProductServices {
	
	@Autowired
	private ProductRepository productRepo;

	public ArrayList<String> listar() {
		List<Products> productos = productRepo.findAll();
		
		ArrayList<String> productNameList = new ArrayList<>();
		for(Products eachProduct : productos) {
			
			productNameList.add(eachProduct.getProductName() );
		}

	    Collections.sort(productNameList);

	    return productNameList;

	}
	
	
}
