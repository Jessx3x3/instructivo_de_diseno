package com.example.tienda;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.junit.jupiter.api.TestInfo;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.tienda.products.ProductRepository;
import com.example.tienda.products.ProductServices;
import com.example.tienda.products.Products;

@RunWith(MockitoJUnitRunner.class)
public class ProductTest {

	@InjectMocks
    private ProductServices productService;
 
    @Mock
    private ProductRepository repo;
    
    @BeforeEach
    public void inicial() {
    	System.out.println("Comienza el testeo");
    }
    @Ignore
    public void repetir() {
    	System.out.println("Repetir test funci�n");
    }
    @AfterEach
    public void termino() {
    	System.out.println("Termina el testeo");
    }
    @Test
    public void testListar_equals() {
    	List<Products> dummyProducts = new ArrayList<>();
    	dummyProducts.add(new Products(null, "Nik", "", 56));
    	dummyProducts.add(new Products(null, "Red Bull", "", 56));

        when(repo.findAll()).thenReturn(dummyProducts);
        
        ArrayList<String> productNameList = new ArrayList<>();
        productNameList.add("Nik");
        productNameList.add("Red Bull");
        
        assertEquals(productNameList, productService.listar());
    }
    @Test
    public void testListar_NotEquals() {
    	List<Products> dummyProducts = new ArrayList<>();
    	dummyProducts.add(new Products(null, "Nik", "", 56));
    	dummyProducts.add(new Products(null, "Red Bull", "", 56));
    	
        when(repo.findAll()).thenReturn(dummyProducts);
        
        ArrayList<String> productNameList = new ArrayList<>();
        productNameList.add("Coca-Cola");
        productNameList.add("Frac");
        
        assertNotEquals(productNameList, productService.listar());
    }
    @Test
    public void testListar_notNull() {
    	List<Products> dummyProducts = new ArrayList<>();
    	
        when(repo.findAll()).thenReturn(dummyProducts);
        
        assertNotNull(productService.listar());
    }
}
